import pickle

def save_model(model, file):
    with open(file, "wb") as f:
        pickle.dump(model, f)

def load_model(file):
    with open(file, "rb") as f:
        return pickle.load(f)