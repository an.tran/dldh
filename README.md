# Deep Learning for Digital Humanities - Poetry II
## Poetry evaluation with Pairwise Preference Learning and Aesthetic Emotions

## Project Structure

```bash
├── preprocessing
│   ├── feature_engineering.py
│   └── data_preprocessing.py
├── utils
│   └── utils.py  
├── src
│   ├── model
│   │     ├── bert-trainer
│   │     ├── GPPL
│   │     └── crowdGPPL
│   ├── train
│   │     ├── train_bert.ipynb
│   │     └── train_GPPL.ipynb
│   └── metrics.ipynb
├── data
│   ├── consolidated_batches.csv
│   ├── Batch_4278643_batch_results_ext.csv
│   └── Batch_4265208_batch_results_main.csv
├── gppl_src <submodule@65eb1cd3>  
├── requirements.txt
└── README.md
```
---

## Contribution 

|Code   | Daniel         | An     | Anissa |
|--------------|-----------|------------|------------|
| ```metrics.ipynb``` | **x**     | **x**        | **x**        |
| `feature_engineering.py`      | x   | **x**       |         |
| `data_preprocessing.py`      | **x**   | **x**       |         |
| `train_bert.ipynb`      | **x**   | x       |         |
| `train_GPPL.ipynb`      | **x**   | **x**       |         |


|Paper   | Daniel         | An     | Anissa |
|--------------|-----------|------------|------------|
| Abstract |    |         |     **x**   | 
| Introduction |     |        | **x**         |
| Related Work      |    |        |    **x**      |
| Data      |    | **x**       |         |
| Methods      | **x**   |        |         |
| Experiments      | **x**   |       |         |
| Discussion      | **x**   | **x**       |         |

**x**: `main contributor`

## Experiments

### Requirements

```bash
pip install -r requirements.txt
```

### Data Analysis

```bash
jupyter-notebook src/metrics.ipynb
```
Contains all experiments conducted during the explorative data analysis (including plots).

### Model Evaluation

```bash
jupyter-notebook src/train/train_bert.ipynb
```
Experiments regarding pairwise preference poetry evaluation with BERT.

```bash
jupyter-notebook src/train/train_GPPL.ipynb
```
Experiments regarding pairwise preference poetry evaluation with GPPL and crowdGPPL.

## Data

### Full Batches Dataset
```bash
data/Batch_4265208_batch_results_main.csv
data/Batch_4278643_batch_results_ext.csv
```

### Consolidated Batches Dataset
```bash
data/consolidated_batches.csv
```

