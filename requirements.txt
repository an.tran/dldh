en-core-web-sm @ https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.1.0/en_core_web_sm-3.1.0-py3-none-any.whl
huggingface-hub==0.4.0
nltk==3.6.7
numpy==1.21.2
pandas==1.3.3
pandocfilters==1.5.0
Pillow==8.4.0
scikit-learn==1.0
scipy==1.7.1
sentence-transformers==2.1.0
sentencepiece==0.1.96
spacy==3.1.3
tokenizers==0.10.3
torch==1.10.1
torchvision==0.11.2
transformers==4.15.0