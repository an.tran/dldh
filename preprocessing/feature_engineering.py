# from textstat import flesch_reading_ease, dale_chall_readability_score, text_standard
from textstat.textstat import textstat
import numpy as np
from typing import List
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
import re 
from g2p_en import G2p
import itertools
import os
from utils.utils import save_model
import pickle 
from hashlib import sha256
from tqdm import tqdm
import logging



def score_readibility(poem:str) -> float:
    """score readability 
    Readability Consensus between various established readability measurements
   

    Args:
        poem (str): the poem

    Returns:
        float: the readability score,  whereby higher scores corresponds with higher text difficulties
    """    
   
    # flesch      = flesch_reading_ease(poem)
    # dale_chall  = dale_chall_readability_score(poem) # readability based on familiar word occurences
    
    return textstat.text_standard(poem, float_output=True)


def get_phonemes(poems:np.array, feature_dir:str="tmp/", save_phonemes_to_memory:str=True):
    def calculate_phonemes(poem:list):
        g2p = G2p()

        stop_words = set(stopwords.words('english'))
    
        lines = [word_tokenize(s) for s in poem]
        lines = [[g2p(word) for word in line if word.lower() not in stop_words and bool(re.match(r"[\d\w]", word))] for line in lines]
        
        return lines


    hashID = sha256()
    phoneme_path = os.path.join(feature_dir, "phonemes")
    if os.path.exists(phoneme_path):
        hashID.update(repr(poems).encode("utf-8"))
        with open(phoneme_path, "rb") as f:
            loaded_data = pickle.load(f)
        if str(hashID.hexdigest()) != loaded_data["hash"]: 
            raise Exception("Specified Phonemes don't match input poems. Specify different 'feature_dir' or set save_phonemes_to_memory 'False'")
        else:
            phonemes = loaded_data["phonemes"] 
    else:
        phonemes = list(map(calculate_phonemes, tqdm(poems.astype(str))))
        if save_phonemes_to_memory:
            logging.info(f"Save Model Phonemes to {phoneme_path}")
            hashID.update(repr(poems).encode("utf-8"))
            with open(phoneme_path, "wb") as f:
                pickle.dump({"phonemes": phonemes, "hash": str(hashID.hexdigest())}, f)
    
    return phonemes


def score_alliteration(poems:np.array):
    """
    assumptions:
    * repetition is no alliteration
    * stopwords may occur between alliterations
    * alliterations are only computed per line
    * alliterations are based on phonetics rather than syntactics
    * alliterations are based on the first syllable of a given word

    Args:
        poem (str): [description]

    Returns:
        float: ratio of alliteration occurences within a poem (doc)
    """    
    phonemes = get_phonemes(poems)


    counter = 0
    for phoneme in phonemes:
        for line in phoneme:
            print(line)
            first_syllables = [p[0] for p in line]
            
            repetitions = [list(v) for _,v in itertools.groupby(first_syllables)]
            repetitions = np.sum([len(repetition) for repetition in repetitions if len(repetition) > 1])

            counter += repetitions
        
    return counter
    