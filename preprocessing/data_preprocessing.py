import pandas as pd
import numpy as np
import ast
import logging
from hashlib import sha256
from typing import Tuple, List
from sentence_transformers import SentenceTransformer
from tqdm import tqdm 
import os
import pickle 
from sklearn.model_selection import train_test_split 


RAW_RESULTS_MAIN:str    = os.path.abspath("data/Batch_4265208_batch_results_main.csv")
RAW_RESULTS_EXT:str     = os.path.abspath("data/Batch_4278643_batch_results_ext.csv")
PROCESSED_RESULTS:str   = os.path.abspath("data/consolidated_batches.csv")

CATEGORIES:List[str]    = ["coherent", "grammatical", "melodious", "moved",	"real",	"rhyming",	"readable",	"comprehensible", "intense", "liking"]	


def load() -> Tuple[pd.DataFrame, pd.DataFrame]:
    """helper function: to read in all *.csv files

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame]: pairwise_df containing the preprocessed dataset, 
        full_pairwise_df containing the raw (full) dataset
    """    
    def replace_with_poem_label(column):
        return column.apply(lambda row: [key for key, val in row.items() if val][0] if type(row) is dict else np.nan)
    
    pairwise_df                     = pd.read_csv(PROCESSED_RESULTS)
    pairwise_df[["poem1", "poem2"]] = pairwise_df[["poem1", "poem2"]].astype(str)
    
    full_pairwise_ext_df    = pd.read_csv(RAW_RESULTS_EXT) #  converters={"Answer.taskAnswers": string2list_converter}
    full_pairwise_main_df   = pd.read_csv(RAW_RESULTS_MAIN)
    full_pairwise_df        = full_pairwise_main_df.append(full_pairwise_ext_df).reset_index(drop=True)

    full_pairwise_df["Answer.taskAnswers"]  = full_pairwise_df["Answer.taskAnswers"].apply(lambda x: ast.literal_eval(x[1:-1].replace("true", "True").replace("false", "False"))) # convert string to dict, replace true/false (str) with True/False (bool)
    criterion = full_pairwise_df["Answer.taskAnswers"].apply(pd.Series)
    criterion = criterion.apply(lambda column: replace_with_poem_label(column), axis=0)
    full_pairwise_df = full_pairwise_df.join(criterion)
    full_pairwise_df.drop(["Answer.taskAnswers"], axis=1, inplace=True)
    
    
    full_pairwise_df.columns = full_pairwise_df.columns.str.replace("-poem", "", regex=True)
    full_pairwise_df.columns = full_pairwise_df.columns.str.replace("Input.", "", regex=True)
    full_pairwise_df[["poem1", "poem2"]]    = full_pairwise_df[["poem1", "poem2"]].astype(str)
    
    
    return pairwise_df, full_pairwise_df


def load_dataset(random_state:int=42, split_ratio:float=0.1) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Load Dataset and conduct basic preprocessing, 
    e.g., adjust datatypes, replace linebreaks

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame]: preprocessed data
    """      

    replace_linebreak       = lambda x: x.str.replace("<br>", r"\n", regex=True) # replace linebreaks
    splitup_linebreak       = lambda x: x.str.split(r"\n")

    pairwise_df, full_pairwise_df = load()

    full_pairwise_df[["poem1", "poem2"]]            = full_pairwise_df[["poem1", "poem2"]].apply(replace_linebreak)
    full_pairwise_df[["poem1_list", "poem2_list"]]  = full_pairwise_df[["poem1", "poem2"]].apply(splitup_linebreak)
    
    ## preprocess pairwise data
    pairwise_df[["poem1","poem2"]]              = pairwise_df[["poem1", "poem2"]].apply(replace_linebreak) 
    pairwise_df[["poem1_list","poem2_list"]]    = pairwise_df[["poem1", "poem2"]].apply(splitup_linebreak)

    return train_test_split(pairwise_df, random_state=random_state, test_size=split_ratio), train_test_split(full_pairwise_df, random_state=random_state, test_size=split_ratio)


def preprocess(df:pd.DataFrame, feature_filter="liking") -> pd.DataFrame:
    """ the data is preprocessed in order to fit the GPPL data format, 
    i.e., poem preferences are mapped to:
    1:  poem1 preferred
    -1: poem2 preferred
    0:  otherwise, e.g., None

    Args:
        df (pd.DataFrame): contains all poems, i.e., poem1, poems2 pair and the corresponding categories

    Returns:
        pd.DataFrame: preprocess dataframes, whereby all poems are assgined to a unique poem id. 
        Same poems receive the same unique id
    """
    
    df.dropna(inplace=True, subset=[feature_filter]) 
    unique_poems = np.unique(df[["poem1", "poem2"]].values.ravel())
    preferences = df[CATEGORIES].astype(np.float64).replace({2: 0}).values  ##.ravel()
    
    index = pd.Index(unique_poems)
    
    a1_poem_idx = index.get_indexer(df.poem1)
    a2_poem_idx = index.get_indexer(df.poem2)

    return unique_poems, preferences, a1_poem_idx, a2_poem_idx
     
def embed_poems(poems:np.array, model:str="all-mpnet-base-v2", save_to_memory:bool=False, embedding_path:str=None) -> np.array:
    """map poems to their corresponding embedding vector, 
    using SentenceTransformer embeddings

    Args:
        poems (pd.Series): containing poem1, poem2

    Returns:
        np.array: mapped embedding vectors
    """
    

    sbert = SentenceTransformer(model)


    if embedding_path:
        hashID = sha256() 

    if embedding_path and os.path.isfile(embedding_path):
        with open(embedding_path, "rb") as f:
            loaded_data = pickle.load(f)
        hashID.update(repr(poems).encode("utf-8"))
        if str(hashID.hexdigest()) != loaded_data["hash"]: 
            raise Exception("Specified Embeddings don't match input poems. Specify different 'embedding_path' or leave it 'None'")
        else:
            embeddings = loaded_data["embeddings"]
    else:
        logging.info(f"Create Sentence Embeddings wiht {model}")
        embeddings = np.array(sbert.encode(poems))
        if save_to_memory:
            if embedding_path:
                logging.info(f"Save Model Embeddings to {embedding_path}")
                hashID.update(repr(poems).encode("utf-8"))
                with open(embedding_path, "wb") as f:
                    pickle.dump({"embeddings": embeddings, "hash": str(hashID.hexdigest())}, f)
            else:
                logging.warning("No 'embedding_path' specified, not saving Embeddings!")
        
    return embeddings

    
def load_and_preprocess_data(save_to_memory:bool=False, return_GPPL_preprocessing:bool=True, embedding_path:str="data/embeddings", min_tasks:int=5, random_state=42, split_ratio=0.1) -> np.array:
    """
    data pipeline to load and preprocess data

    Returns:
        np.array: poem embdedings 
    """
        
    (pairwise_ds_train, pairwise_ds_test), (full_pairwise_ds_train, full_pairwise_ds_test) = load_dataset(random_state=random_state, split_ratio=split_ratio)

    output = []

    for pairwise_ds, full_pairwise_ds in zip((pairwise_ds_train, pairwise_ds_test), (full_pairwise_ds_train, full_pairwise_ds_test)):
    
        tmp     = full_pairwise_ds["WorkerId"].value_counts()
        user_df = pd.DataFrame({"WorkerId":     tmp.index,
                                "tasks_taken":  tmp.values})

        worker_ids = user_df["WorkerId"][user_df["tasks_taken"] >= min_tasks]

        full_pairwise_ds = full_pairwise_ds[full_pairwise_ds["WorkerId"].isin(worker_ids)]
        
        
        pairwise_path = (None, os.path.join(embedding_path, "pairwise"))[bool(embedding_path)]
        full_pairwise_path = (None, os.path.join(embedding_path, "full_pairwise"))[bool(embedding_path)]

        output_step = []

        poem_embed = None

        for data_df, embed_path in zip((pairwise_ds, full_pairwise_ds), (pairwise_path, full_pairwise_path)):
            # preprocess dataset
            unique_poems, preferences, a1_poem_idx, a2_poem_idx = preprocess(data_df)

            #if poem_embed is None:
            unique_poems_fixed = unique_poems # fixed ordering (for consistency)
            poem_embed = embed_poems(unique_poems_fixed, save_to_memory=save_to_memory, embedding_path=embed_path)

            if return_GPPL_preprocessing:
                gppl_format = [unique_poems_fixed, preferences, a1_poem_idx, a2_poem_idx, poem_embed]
                if "WorkerId" in data_df.columns:
                    gppl_format.append(data_df["WorkerId"].astype("category").cat.codes.values)
                output_step.append(tuple(gppl_format))
            else:    
            # data_df["embedding"] = poem_embed
                poem_df = pd.DataFrame(list(zip(unique_poems_fixed, poem_embed)), columns=["unique_poem", "embedding"])
                result_df = data_df.merge(poem_df, left_on="poem1", right_on="unique_poem", how='left')
                result_df = result_df.merge(poem_df, left_on="poem2", right_on="unique_poem", suffixes=["1","2"], how='left')
                result_df = result_df.loc[:,~result_df.columns.str.startswith('unique_poem')]
                output_step.append(result_df)

        output.append(tuple(output_step))

    return output


def preprocess_for_crowdGPPL(preferences, a1_poem_idx, a2_poem_idx):
    n_data, n_pref = preferences.shape
    new_pref = preferences.astype(int).flatten()
    a1_poem_idx_new = np.repeat(a1_poem_idx, n_pref)
    a2_poem_idx_new = np.repeat(a2_poem_idx, n_pref)
    person_ids = np.concatenate(tuple(np.ones(n_data, dtype=np.int32) * personID for personID in range(0, n_pref)))

    return new_pref, a1_poem_idx_new, a2_poem_idx_new, person_ids


